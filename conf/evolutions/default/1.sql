# Tasks SCHEMA

# --- !Ups
CREATE SEQUENCE task_id_seq;
CREATE TABLE tasks (
  id      INTEGER NOT NULL DEFAULT nextval('task_id_seq'),
  title   VARCHAR(255),
  content VARCHAR(255),
  color   VARCHAR(10)
);
CREATE SEQUENCE labels_id_seq;
CREATE TABLE labels (
  id    INTEGER NOT NULL DEFAULT nextval('labels_id_seq'),
  title VARCHAR(255) UNIQUE
);
CREATE SEQUENCE tasks_to_labels_id_seq;
CREATE TABLE tasks_to_labels (
  id      INTEGER NOT NULL DEFAULT nextval('tasks_to_labels_id_seq'),
  idTask  INTEGER,
  idLabel INTEGER
);

# --- !Downs
DROP TABLE tasks;
DROP SEQUENCE task_id_seq;
DROP TABLE labels;
DROP SEQUENCE labels_id_seq;
DROP TABLE tasks_to_labels;
DROP SEQUENCE tasks_to_labels_id_seq;