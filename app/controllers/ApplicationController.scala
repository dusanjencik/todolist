package controllers

import models._
import play.api.libs.json.{JsValue, Json, Writes}
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ApplicationController extends Controller {

  def index = Action.async { implicit request =>
    TasksToLabels.getTasksWithLabels.map(println)
    TasksToLabels.getTasksWithLabels.map(data => Ok(views.html.index(data)))
  }

  implicit val taskJson = new Writes[Task] {
    override def writes(task: Task): JsValue = {
      Json.obj(
        "id" -> task.id,
        "title" -> task.title,
        "content" -> task.content,
        "color" -> task.color
      )
    }
  }

  implicit val labelJson = new Writes[Label] {
    override def writes(label: Label): JsValue = Json.obj(
      "id" -> label.id,
      "title" -> label.title
    )
  }

  implicit val taskToLabelJson = new Writes[TaskToLabel] {
    override def writes(taskToLabel: TaskToLabel): JsValue = Json.obj(
      "id" -> taskToLabel.id,
      "idTask" -> taskToLabel.idTask,
      "idLabel" -> taskToLabel.idLabel
    )
  }

  implicit val taskWithLabelJson = new Writes[TaskWithLabelsWrapper] {
    override def writes(taskWithLabelsWrapper: TaskWithLabelsWrapper): JsValue = {
      Json.obj(
        "task" -> taskWithLabelsWrapper.task,
        "labels" -> Json.toJson(taskWithLabelsWrapper.labels)
      )
    }
  }

  private def updateTask(f: Task => Future[Task], g: TaskToLabel => Future[TaskToLabel]) = Action.async {
    implicit request =>
      TaskForm.form.bindFromRequest.fold(
        errorForm => Future.successful(BadRequest),
        data => {
          f(Task(data._1, data._2, data._3, data._4)).map {
            task => {
              TasksToLabels.deleteAllToTask(task.id)
              data._5.foreach(idLabel => TasksToLabels.add(TaskToLabel(0, task.id, idLabel)))
              val labels: Seq[Label] = for (idLabel <- data._5)
                yield Labels.getNow(idLabel)
              println(task)
              Ok(Json.toJson(TaskWithLabelsWrapper(task, labels)))
            }
          }
        }
      )
  }

  def addTask(): Action[AnyContent] = updateTask(Tasks.add, TasksToLabels.add)

  def updateTask(): Action[AnyContent] = updateTask(Tasks.update, TasksToLabels.add)

  def deleteTask(id: Long) = Action.async {
    implicit request => {
      TasksToLabels.deleteAllToTask(id)
      Tasks.delete(id).map(res => Ok(Json.toJson(res)))
    }
  }

  def getTask(id: Long) = Action.async {
    implicit request =>
      Tasks.get(id).map(t => Ok(Json.toJson(t.getOrElse(Tasks.nullObject))))
  }

  def getTaskWithLabels(id: Long) = Action.async {
    implicit request =>
      Tasks.getWithLabels(id).map(t => Ok(Json.toJson(t)))
  }

  def getTaskList = Action.async {
    implicit request =>
      Tasks.listAll().map(res => Ok(Json.toJson(res)))
  }

  private def updateLabel(f: Label => Future[Label]) = Action.async {
    implicit request =>
      LabelForm.form.bindFromRequest.fold(
        errorForm => Future.successful(BadRequest),
        data => f(data).map {
          label => {
            println(label)
            Ok(Json.toJson(label))
          }
        }
      )
  }

  def addLabel(): Action[AnyContent] = updateLabel(Labels.add)

  def updateLabel(): Action[AnyContent] = updateLabel(Labels.update)

  def deleteLabel(id: Long) = Action.async {
    implicit request => {
      TasksToLabels.deleteAllToLabel(id)
      Labels.delete(id).map(res => Ok(Json.toJson(res)))
    }
  }

  def getLabel(id: Long) = Action.async {
    implicit request =>
      Labels.get(id).map(res => Ok(Json.toJson(res)))
  }

  def getLabelList = Action.async {
    implicit request =>
      Labels.listAll().map(res => Ok(Json.toJson(res)))
  }

  private def updateTaskToLabel(f: TaskToLabel => Future[TaskToLabel]) = Action.async {
    implicit request =>
      TaskToLabelForm.form.bindFromRequest.fold(
        errorForm => Future.successful(BadRequest),
        data => f(data).map {
          taskToLabel => {
            println(taskToLabel)
            Ok(Json.toJson(taskToLabel))
          }
        }
      )
  }

  def addTaskToLabel(): Action[AnyContent] = updateTaskToLabel(TasksToLabels.add)

  def updateTaskToLabel(): Action[AnyContent] = updateTaskToLabel(TasksToLabels.update)

  def deleteTaskToLabel(id: Long) = Action.async {
    implicit request =>
      TasksToLabels.delete(id).map(res => Ok(Json.toJson(res)))
  }

  def getTaskToLabel(id: Long) = Action.async {
    implicit request =>
      TasksToLabels.get(id).map(res => Ok(Json.toJson(res)))
  }

  def getTaskToLabelList = Action.async {
    implicit request =>
      TasksToLabels.listAll().map(res => Ok(Json.toJson(res)))
  }

}