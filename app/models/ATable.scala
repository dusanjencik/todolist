package models

import slick.driver.H2Driver.api._

/**
  * @author Dušan Jenčík dusanjencik@gmail.com 
  *         created 31.05.16
  */
abstract class ATable[T](tag: Tag, tableName: String) extends Table[T](tag, tableName) {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
}