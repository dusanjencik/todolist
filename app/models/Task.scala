package models

import play.api.data.Form
import play.api.data.Forms._
import slick.driver.H2Driver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * @author Dušan Jenčík dusanjencik@gmail.com 
  *         created 21.05.16
  */

object Color extends Enumeration {
  type Color = Value
  val RED = Value("red")
  val PURPLE = Value("purple")
  val YELLOW = Value("yellow")
  val GREEN = Value("green")
  val BLUE = Value("blue")
}

case class Task(id: Long, title: String, content: String, color: String)

object TaskForm {
  val form = Form(
    tuple(
      "id" -> longNumber,
      "title" -> text,
      "content" -> nonEmptyText,
      "color" -> nonEmptyText,
      "label" -> list(longNumber)
    )
  )
}

class TasksTable(tag: Tag) extends ATable[Task](tag, "tasks") {
  def title = column[String]("title")

  def content = column[String]("content")

  def color = column[String]("color")

  override def * = (id, title, content, color) <>(Task.tupled, Task.unapply)
}

object Tasks extends AEntityDBProvider[Task, TasksTable](TableQuery[TasksTable]) {

  def add(task: Task): Future[Task] = {
    add(
      tableQuery.map(c => (c.title, c.content, c.color))
        returning tableQuery.map(_.id) +=
        (task.title, task.content, task.color)
    )(Task(_, task.title, task.content, task.color))
  }

  def update(task: Task): Future[Task] = {
    update(
      filter(task.id).map(c => (c.title, c.content, c.color))
        .update(task.title, task.content, task.color)
    )(_ => Task(task.id, task.title, task.content, task.color))
  }

  def getWithLabels(id: Long) = {
    Future(TaskWithLabelsWrapper(getNow(get(id)).get,
      for {
        join <- getNow(TasksToLabels.getTaskToLabels(id))
        label <- getNow(Labels.get(join.idLabel))
      } yield label)
    )
  }

}