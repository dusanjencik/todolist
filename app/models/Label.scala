package models

import play.api.data.Form
import play.api.data.Forms._
import slick.driver.H2Driver.api._
import slick.lifted.ColumnOrdered

import scala.concurrent.Future

/**
  * @author Dušan Jenčík dusanjencik@gmail.com 
  *         created 31.05.16
  */
case class Label(id: Long, title: String)

object LabelForm {
  val form = Form(
    mapping(
      "id" -> longNumber,
      "title" -> nonEmptyText
    )(Label.apply)(Label.unapply)
  )
}

class LabelsTable(tag: Tag) extends ATable[Label](tag, "labels") {
  def title = column[String]("title")

  override def * = (id, title) <>(Label.tupled, Label.unapply)
}

object Labels extends AEntityDBProvider[Label, LabelsTable](TableQuery[LabelsTable]) {

  def add(label: Label): Future[Label] = {
    add(
      tableQuery.map(_.title) returning tableQuery.map(_.id) += label.title
    )(Label(_, label.title))
  }

  def update(label: Label): Future[Label] = {
    update(
      filter(label.id).map(_.title).update(label.title)
    )(_ => Label(label.id, label.title))
  }

  def getNow(id: Long): Label = {
    getNow(get(id)).get
  }

  override def listAll(sortType: (LabelsTable) => ColumnOrdered[Long] = _.id): Future[Seq[Label]] = super.listAll(sortType)
}