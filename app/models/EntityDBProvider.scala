package models

import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import slick.dbio.{DBIOAction, NoStream}
import slick.driver.H2Driver.api._
import slick.driver.JdbcProfile
import slick.lifted.{ColumnOrdered, TableQuery}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * @author Dušan Jenčík dusanjencik@gmail.com 
  *         created 31.05.16
  */
object EntityDBProvider {
  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
}

abstract class AEntityDBProvider[Entity, EntityTable <: ATable[Entity]](val tableQuery: TableQuery[EntityTable]) {
  val dbConfig = EntityDBProvider.dbConfig

  val nullObject: Entity = null.asInstanceOf[Entity]

  def query[R](a: DBIOAction[R, NoStream, Nothing]): Future[R] = dbConfig.db.run(a)

  protected def add[R](q: DBIOAction[R, NoStream, Nothing])(creator: R => Entity) = {
    query(q).map(res => {
      println(this.getClass.getSimpleName + " successfully added")
      creator(res)
    }).recover {
      case ex: Exception => println(ex.getCause.getMessage)
        nullObject
    }
  }

  protected def update[R](q: DBIOAction[R, NoStream, Nothing])(creator: R => Entity) = {
    query(q).map(res => {
      println(this.getClass.getSimpleName + " successfully updated")
      creator(res)
    }).recover {
      case ex: Exception => println(ex.getCause.getMessage)
        nullObject
    }
  }

  def delete(id: Long): Future[Int] = {
    query(filter(id).delete)
  }

  def get(id: Long): Future[Option[Entity]] = {
    query(filter(id).result.headOption)
  }

  def listAll(sortType: EntityTable => ColumnOrdered[Long] = _.id.desc): Future[Seq[Entity]] = {
    query(tableQuery.sortBy(sortType).result)
  }

  def filter(id: Long) = tableQuery.filter(_.id === id)

  protected def getNow[B](future: Future[B]) = Await.ready(future, Duration.Inf).value.get.get

}