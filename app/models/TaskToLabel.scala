package models

import play.api.data.Form
import play.api.data.Forms._
import slick.driver.H2Driver.api._
import slick.lifted.TableQuery

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * @author Dušan Jenčík dusanjencik@gmail.com 
  *         created 31.05.16
  */
case class TaskToLabel(id: Long, idTask: Long, idLabel: Long)

case class TaskWithLabelsWrapper(task: Task, labels: Seq[Label])

case class LabelWithTasksWrapper(label: Label, tasks: Seq[Task])

class TaskToLabelTable(tag: Tag) extends ATable[TaskToLabel](tag, "tasks_to_labels") {
  def idTask = column[Long]("idTask")

  def idLabel = column[Long]("idLabel")

  override def * = (id, idTask, idLabel) <>(TaskToLabel.tupled, TaskToLabel.unapply)

  def idTaskFk = foreignKey("idTask_fk", idTask, Tasks.tableQuery)(_.id)

  def idLabelFk = foreignKey("idLabel_fk", idLabel, Labels.tableQuery)(_.id)
}

object TaskToLabelForm {
  val form = Form(
    mapping(
      "id" -> longNumber,
      "idTask" -> longNumber,
      "idLabel" -> longNumber
    )(TaskToLabel.apply)(TaskToLabel.unapply)
  )
}

object TasksToLabels extends AEntityDBProvider[TaskToLabel, TaskToLabelTable](TableQuery[TaskToLabelTable]) {

  def add(taskToLabel: TaskToLabel): Future[TaskToLabel] = {
    add(
      tableQuery.map(c => (c.idTask, c.idLabel)) returning tableQuery.map(_.id) +=
        (taskToLabel.idTask, taskToLabel.idLabel)
    )(TaskToLabel(_, taskToLabel.idTask, taskToLabel.idLabel))
  }

  def update(taskToLabel: TaskToLabel): Future[TaskToLabel] = {
    update(
      filter(taskToLabel.id).map(c => (c.idTask, c.idLabel)).update(taskToLabel.idTask, taskToLabel.idLabel)
    )(_ => TaskToLabel(taskToLabel.id, taskToLabel.idTask, taskToLabel.idLabel))
  }

  def deleteAllToTask(idTask: Long) = {
    getNow(query {
      println("delete all for " + idTask)
      tableQuery.filter(_.idTask === idTask).delete
    })
  }

  def deleteAllToLabel(idLabel: Long) = {
    getNow(query {
      println("delete all for " + idLabel)
      tableQuery.filter(_.idLabel === idLabel).delete
    })
  }

  def getTaskToLabels(idTask: Long) = {
    query(
      tableQuery.filter(_.idTask === idTask).result
    )
  }

  def getTasksWithLabels = {
    Future(getNow(Tasks.listAll()).map(task => TaskWithLabelsWrapper(task,
      for {
        join <- getNow(listAll())
        label <- getNow(Labels.listAll())
        if task.id == join.idTask && join.idLabel == label.id
      } yield label)
    ))
  }
}