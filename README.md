# TODO list #

Semestrální projekt na předmět Programování ve Scale.

Tato práce implementuje funkcionalitu TODO listu (poznámkového bloku) v jazycích Scala, HTML, CSS, JavaScript.
V aplikaci lze vytvářet nové poznámky, přidávat k nim štítky či je obarvovat. Všechny prvky lze interaktivně editovat.

Bylo využito frameworku Play a databáze H2 (lokální).