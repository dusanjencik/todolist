$(document).ready(function () {
    loadLabelList();
    Materialize.updateTextFields();
    $('select').material_select();
    $('.modal-trigger').leanModal();
});

var $grid = $('#grid').packery({
    itemSelector: '.grid-item',
    gutter: 10
});
var $labels = $('#labels');
var $labelInputs = $('#labels-add');
var $labelUpdate = $('#labels-update')
var taskRoute = "/tasks";
var labelRoute = "/labels";

function resetNewTask() {
    $('#title-new-task').val('');
    $('#content-new-task').val('');
}

// get task
$grid.on('click', '.grid-item', function (event) {
    $.ajax({
        type: 'GET',
        url: taskRoute + '/getWithLabels/' + $(this).attr('data-id'),
        success: function (data) {
            if (data == "{}") {
                alert("error " + data);
                return;
            }
            $('#title-update').val(data.task.title);
            $('#content-update').val(data.task.content);
            $('#updated-id').val(data.task.id);
            $('#color_' + data.task.color).prop('checked', true);
            $labelUpdate.find('input').prop('checked', false);
            for (var i in data.labels) {
                $labelUpdate.find("input[data-id=" + data.labels[i].id + "]").prop('checked', true);
            }
            $('#update-task-modal').openModal();
        },
        error: function () {
            alert("Error in getting");
        }
    })
});

// get label
$labels.on('click', '.label-item', function (event) {
    $.ajax({
        type: 'GET',
        url: labelRoute + '/get/' + $(this).attr('data-id'),
        success: function (data) {
            if (data == "{}") {
                alert("error " + data);
                return;
            }
            $('#title-label-update').val(data.title);
            $('#updated-label-id').val(data.id);
        },
        error: function () {
            alert("Error in getting");
        }
    })
});

// get labelList
$('#load-labels').click(function (event) {
    loadLabelList();
});

// delete task
$grid.on('click', '.delete', function (event) {
    event.stopPropagation();
    var parent = $(this).parent().parent();
    // alert(parent);
    $.ajax({
        type: 'DELETE',
        url: taskRoute + '/delete/' + $(this).attr('data-id'),
        success: function () {
            $grid.packery('remove', parent).packery();
        },
        error: function () {
            alert("Error in deleting");
        }
    })
});

// delete label
$labels.on('click', '.delete-label', function (event) {
    event.stopPropagation();
    $.ajax({
        type: 'DELETE',
        url: labelRoute + '/delete/' + $(this).attr('data-id'),
        success: function () {
            loadLabelList();
        },
        error: function () {
            alert("Error in deleting");
        }
    })
});

// add task
$('#task-data-form').on("submit", function (event) {
    event.preventDefault(event);
    $.ajax({
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data: $(this).serialize(),
        success: function (data) {
            var item = $(createGridItem(data));
            $grid.prepend(item).packery('prepended', item);
            resetNewTask();
        },
        error: function () {
            alert("Error in adding");
        }
    });
});

// add label
$('#label-data-form').on("submit", function (event) {
    event.preventDefault(event);
    $.ajax({
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data: $(this).serialize(),
        success: function (data) {
            loadLabelList();
            $('#title-new-label').val("");
        },
        error: function () {
            alert("Error in adding");
        }
    });
});

// update task
$('#task-data-form-update').on("submit", function (event) {
    event.preventDefault(event);
    var id = $('#updated-id').val();
    var gridItem = $grid.find(".grid-item[data-id=" + id + "]");
    $.ajax({
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data: $(this).serialize(),
        success: function (data) {
            gridItem.attr("data-id", data.task.id);
            gridItem.attr("class", 'card hoverable grid-item ' + data.task.color + ' lighten-3');
            gridItem.html(createGridItemContent(data));
        },
        error: function () {
            alert("Error in changing");
        }
    });
});

// update label
$('#label-data-form-update').on("submit", function (event) {
    event.preventDefault(event);
    $.ajax({
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data: $(this).serialize(),
        success: function (data) {
            loadLabelList();
        },
        error: function () {
            alert("Error in changing");
        }
    });
});

function loadLabelList() {
    $.ajax({
        type: 'GET',
        url: labelRoute + '/list',
        success: function (data) {
            var content = "";
            var inputContent = "";
            var updateContent = "";
            for (var i in data) {
                content += createLabelItem(data[i]);
                inputContent += createLabelCheckbox(data[i], i, "input");
                updateContent += createLabelCheckbox(data[i], i, "update");
            }
            $labels.html(content);
            $labelInputs.html(inputContent);
            $labelUpdate.html(updateContent);
            $('.modal-trigger').leanModal();
        },
        error: function () {
            alert("Error in loading");
        }
    })
}

function createGridItem(data) {
    return '<div data-id="' + data.task.id + '" class="card hoverable grid-item ' + data.task.color + ' lighten-3">' +
        createGridItemContent(data) + '</div>';
}

function createGridItemContent(data) {
    var output = '<b class="grid-item-title">' + data.task.title + '</b> <br>' + data.task.content +
        '<br>';
    for (var i in data.labels) {
        output += '<div data-id="' + data.labels[i].id + '" class="label-flag">' + data.labels[i].title + '</div>';
    }
    output += '<div class="grid-item-bar">' +
        '<a href="#!" data-id="' + data.task.id + '" class="delete"><i class="material-icons">delete</i></a>' +
        '</div>';
    return output;
}

function createLabelItem(data) {
    return '<div data-id="' + data.id + '">' + createLabelItemContent(data) + '</div>';
}

function createLabelItemContent(data) {
    return '<a href="#!" data-id="' + data.id + '" class="delete-label"><i class="material-icons">delete</i></a>' + data.title +
        '<a href="#update-label-modal" data-id="' + data.id + '" class="label-item modal-trigger"><i class="material-icons">mode_edit</i></a>';
}

function createLabelCheckbox(data, index, prefix) {
    return '<p>' +
        '<input data-id="' + data.id + '" id="' + prefix + '_chck-label-' + data.id + '" type="checkbox" name="label[' + index + ']" value="' + data.id + '">' +
        '<label for="' + prefix + '_chck-label-' + data.id + '">' + data.title + ' </label></p>';
}

$("textarea").on("keypress", function (e) {
   if(e.which === 13) {
       $(this).val($(this).val() + "<br>");
   }
});